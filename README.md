# About
This is an implementation of the 'yes' program.

At first I was curious to see how fast it would be (compared to GNU yes) using a very straightforward and simple implementation.

I've seen GNU yes output at around 10Gbps.
The very first version of my program output at about 5Mbps, which was a lot slower than I initially thought. After adding a buffer, it sped up to about 4.5Gpbs. Obviously that's what makes the largest difference.

I do not plan on adding any further optimizations. 

# Benchmark the speed
/path/to/my/binary/yes | pv > /dev/null
