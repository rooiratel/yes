use std::env;
use std::io::{stdout, Write};

fn main() {
    let mut y = String::from("y");
    let mut args = String::from("");

    for arg in env::args().skip(1) {
        args = format!("{} {}", args, arg);
    }

    if !args.is_empty() {
        y = args;
    }

    let buffer = y.repeat(4096).into_bytes();
    let out = stdout();
    let mut stdout = out.lock();

    loop {
        stdout.write(&buffer).unwrap();
    }
}
